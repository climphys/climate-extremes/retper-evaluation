#!/bin/bash

###### HEADER - 1_Reanalysis_Preprocessing.sh ################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    05.12.2021
# Purpose: With this script, all ERA5 data is pre-processed to be analysed
#          using R.
##############################################################################

## Overwrite existing files? / Print additional information?
overwrite=FALSE
verbose=TRUE

## Define path to config file:
model_info_file=/home/jzeder/Projects/gev-issues/scripts/1_data_prep/1_Reanalysis_Preprocessing.conf

## Define working directory path:
wd_path=$( grep ^"wd_path"\| $model_info_file | cut -d"|" -f2)
mkdir -pv $wd_path
cd $wd_path

## Files with grid information:
grid_cmip6ng_orig=/home/lukbrunn/Documents/Git/cmip6-ng/grids/g025.txt
grid_cesm1le_orig=/net/h2o/climphys/jzeder/Projects/nonstationary-extremes/data/Tx7d/CMIP6/prepare_CMIP6/griddes.txt
grid_era5_processed=/net/atmos/data/ERA5_deterministic/recent/0.25deg_lat-lon_1m/processed/regrid/scripts/mygrid
grid_cmip6ng=$wd_path/_helper_files/grid_cmip6ng.txt
grid_cesm1le=$wd_path/_helper_files/grid_cesm1le.txt
grid_era5=$wd_path/_helper_files/grid_era5.txt

## Input reanalysis file location:
path_ERA5_in=/net/atmos/data/ERA5_deterministic/recent/
path_ERA5Land_in=/net/atmos/data/ERA5-Land/recent/

## Additional scripts used:
bashscript_functions=~/Projects/nonstationary-extremes/scripts/1_CMIP6_Preprocessing_FUN.sh
smooth_tas_ensmean_script=~/Projects/nonstationary-extremes/scripts/1_smooth_tas_ensmean.R
seas_basestate_tas_script=~/Projects/nonstationary-extremes/scripts/1_seas_basestate_tas.R
seas_scaling_script=~/Projects/nonstationary-extremes/scripts/1_seas_scaling.R

#### DEFINE FUNCTIONS ########################################################
## Copy grid files:
source $bashscript_functions
module load cdo/1.9.9

#### MAIN BODY ###############################################################
## Get grid information (the first time):
if [[ ! -f $grid_cmip6ng && ! -f $grid_era5 ]]; then
    copy_grid_files
fi

## Models to be looped over:
if [ $# -eq 0 ]
  then
  MODELS=$( grep ^"Models"\| $model_info_file | cut -d"|" -f2)
  get_variable_tres_list
else
  MODELS=$1
  VARIABLES=$2
  if [[ "$VARIABLES" == "All" || "$VARIABLES" == "ALL" ]]; then
    VARIABLES=$( grep ^"Variables"\| $model_info_file | cut -d"|" -f2)
  fi
  get_variable_tres_list
fi
echo ""
echo ""
echo "      === Preprocessing data for models $MODELS and variables $VARIABLES ==="
echo "          Variables: $VARIABLES_DAY (day) / $VARIABLES_MON (mon)"
echo ""
echo ""
sleep 3s

## Define runmean-length [days]:
RUNMEAN_LEN=$( grep ^"runmean_len"\| $model_info_file | cut -d"|" -f2)

## Set up directory structure:
mkdir -p areacella/fx/g025/ sftlf/fx/g025/ orog/fx/g025/
for VAR in $VARIABLES_DAY; do
  mkdir -p $VAR/day/g025/_checkplot/
done
for VAR in $VARIABLES_MON; do
  mkdir -p $VAR/mon/g025/_checkplot/
done

## Loop over existing CMIP6ng datasets:
echo "      ----------- Retrieve reanalysis data -----------"
for MODEL in $MODELS
do
  if [ "$verbose" = "TRUE" ]; then echo "Working on model: $MODEL"; fi

  ## Get model-specific information:
  mod_lname=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f2 )
  mod_setting=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f3 )
  mod_exp_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f4 )
  mod_ensind_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f5 )
  mod_soil_weight=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f6 )

  ## Get gridpoint size in 2.5° resolution:
  get_fixed_var_ERA5
  
  ## Loop over variables:
  for VAR in $VARIABLES
  do
    if [ "$verbose" = "TRUE" ]; then echo "  - Working on variable: $VAR"; fi

    ## Get temporal/spatial resolution for different variables:
    tres_in=$( grep ^$VAR\| $model_info_file | cut -d"|" -f2 )
    ERA5_name=$( grep ^$VAR\| $model_info_file | cut -d"|" -f3 )

    ## Loop over experiments:
    for EXP in $mod_exp_list
    do
      if [ "$verbose" = "TRUE" ]; then echo "    - Working on experiment: $EXP"; fi
      ens_i=1
      get_runmean_ERA5

    done

  done

done

## Detrend seasonal means of mrsol and zg500:
echo " ----------- Detrend mrsol/zg500 -----------"
for MODEL in $MODELS
do
  if [ "$verbose" = "TRUE" ]; then echo "Working on model: $MODEL"; fi

  ## Get model-specific information:
  mod_lname=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f2 )
  mod_setting=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f3 )
  mod_exp_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f4 )
  mod_ensind_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f5 )
  mod_startend_year=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f6 )

  ## Loop over variables:
  for VAR in $VARIABLES
  do
    if [ "$verbose" = "TRUE" ]; then echo "  - Working on variable: $VAR"; fi

    ## Get temporal/spatial resolution for different variables:
    tres_in=$( grep ^$VAR\| $model_info_file | cut -d"|" -f2 )

    ## Get base state and linear trend with smoothed annual GMST
    ens_i=1
    get_seasonal_scaling_pattern_reanalysis
  done

done

## Rename t2m files such that event temperature are taken from ERA5-Land
## dataset:
filepath_out=${wd_path}/tas/day/g025/
filename_out_ERA=tas_day_ERA5_reanalysis_r1i1p1_g025.RUNMEAN${RUNMEAN_LEN}.nc
filename_out_ERALand=tas_day_ERA5_reanalysis_r1i1p1_g025.RUNMEAN${RUNMEAN_LEN}.nc.ERA5Land
if [[ -f $filepath_out/$filename_out_ERA ]]; then
  mv $filepath_out/$filename_out_ERA $filepath_out/$filename_out_ERA.ERA5Det
fi
if [[ -f $filepath_out/$filename_out_ERALand ]]; then
  mv $filepath_out/$filename_out_ERALand $filepath_out/$filename_out_ERA
fi

## Send notification email:
duration=$SECONDS
mail -s "Retrieving 7-day running mean for CMIP6 models $MODELS and $VARIABLES ($(($duration / 60))min $(($duration % 60))s)" joel.zeder@env.ethz.ch < /dev/null
exit 0









