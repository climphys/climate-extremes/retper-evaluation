#!/bin/bash

###### HEADER - 1_CESM2_Preprocessing.sh #####################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    12.04.2022
# Purpose: With this script, all CESM12 data is pre-processed to be analysed
#          using R.
##############################################################################

## Overwrite existing files? / Print additional information?
overwrite=FALSE
verbose=TRUE

## Define path to config file:
model_info_file=/home/jzeder/Projects/gev-issues/scripts/1_data_prep/1_CESM2_Preprocessing.conf

## Define working directory path:
wd_path=$( grep ^"wd_path"\| $model_info_file | cut -d"|" -f2)
mkdir -pv $wd_path
cd $wd_path

## Files with grid information:
grid_era5_processed=/net/atmos/data/ERA5_deterministic/recent/0.25deg_lat-lon_1m/processed/regrid/scripts/mygrid
grid_cmip6ng=/net/xenon/climphys/jzeder/Projects/nonstationary-extremes/data/Tx7d/CMIP6/_helper_files/grid_cmip6ng.txt
grid_cesm1le=/net/xenon/climphys/jzeder/Projects/nonstationary-extremes/data/Tx7d/CMIP6/_helper_files/grid_cesm1le.txt
# grid_era5=$wd_path/_helper_files/grid_era5.txt

## Input CMIP6 file location:
# path_cmip6ng_in=/net/atmos/data/cmip6-ng/
# path_cmip6_in=/net/atmos/data/cmip6/

## Additional scripts used:
bashscript_functions=~/Projects/nonstationary-extremes/scripts/1_CMIP6_Preprocessing_FUN.sh
smooth_tas_ensmean_script=~/Projects/nonstationary-extremes/scripts/1_smooth_tas_ensmean.R
seas_basestate_tas_script=~/Projects/nonstationary-extremes/scripts/1_seas_basestate_tas.R
seas_scaling_script=~/Projects/nonstationary-extremes/scripts/1_seas_scaling.R

#### DEFINE FUNCTIONS ########################################################
## Copy grid files:
source $bashscript_functions

#### MAIN BODY ###############################################################
## Get grid information (the first time):
if [[ ! -f $grid_cmip6ng && ! -f $grid_cesm1le ]]; then
    copy_grid_files
fi

## Models to be looped over:
if [ $# -eq 0 ]
  then
  MODELS=$( grep ^"Models"\| $model_info_file | cut -d"|" -f2)
  get_variable_tres_list
else
  MODELS=$1
  VARIABLES=$2
  if [[ "$VARIABLES" == "All" || "$VARIABLES" == "ALL" ]]; then
    VARIABLES=$( grep ^"Variables"\| $model_info_file | cut -d"|" -f2)
  fi
  get_variable_tres_list
fi
echo ""
echo ""
echo "      === Preprocessing data for models $MODELS and variables $VARIABLES ==="
echo "          Variables: $VARIABLES_DAY (day) / $VARIABLES_MON (mon)"
echo ""
echo ""
sleep 3s

## Define runmean-length [days]:
RUNMEAN_LEN=$( grep ^"runmean_len"\| $model_info_file | cut -d"|" -f2)

## Set up directory structure:
mkdir -p areacella/fx/g025/ sftlf/fx/g025/ orog/fx/g025/
for VAR in $VARIABLES_DAY; do
  mkdir -p $VAR/day/g025/_checkplot/
done
for VAR in $VARIABLES_MON; do
  mkdir -p $VAR/mon/g025/_checkplot/
done


## Get day runmean CESM12-LE data:
get_runmean_CESM2 () {
  ## Should running mean be calculated? Not in case of mrsol variable:
  if [ "$VAR" == "mrsol" ]; then
    nameext=""
    rmlen=1
  else
    nameext=.RUNMEAN${RUNMEAN_LEN}
    rmlen=$RUNMEAN_LEN
  fi
  
  ## Get input filepath:
  # filepath_in=${wd_path}/${VAR}/${tres_in}/native/
  filepath_in=/net/xenon/climphys/jzeder/Projects/gev-issues/data/CESM2/${VAR}/${tres_in}/native/
  
  ## Get input and output filenames:
  filepath_out=${wd_path}/${VAR}/${tres_in}/g025/
  filename_out=${VAR}_${tres_in}_${mod_lname}_${EXP}_r${ens_i}i${mod_setting}_g025${nameext}.nc
  file_out=$filepath_out$filename_out
  
  ## Check whether output file already exists (and if not overwrite, return):
  if [[ ! -f $file_out || "$overwrite" == "TRUE" ]]
  then
    if [ "$verbose" == "TRUE" ]; then echo "       Create file $filename_out"; fi
    if [[ "$VAR" == "tas" && "$RUNMEAN_LEN" == "7" ]]; then
      file_in=$filepath_in/tas_day_CESM2-LE_${EXP}_r${ens_i}i1p1_native.RUNMEAN7.nc
      cdo -s setname,$VAR, -sellonlatbox,197,280,27,68, -remapcon2,$grid_cmip6ng, -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "tas" && "$RUNMEAN_LEN" == "5" ]]; then
      file_in=$filepath_in/tas_day_CESM2-LE_${EXP}_r${ens_i}i1p1_native.RUNMEAN5.nc
      cdo -s setname,$VAR, -sellonlatbox,197,280,27,68, -remapcon2,$grid_cmip6ng, -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "tas" && "$RUNMEAN_LEN" == "1" ]]; then
      file_in=$filepath_in/tas_day_CESM2-LE_${EXP}_r${ens_i}i1p1_native.nc
      cdo -s setname,$VAR, -sellonlatbox,197,280,27,68, -remapcon2,$grid_cmip6ng, -selvar,$CESM12_name $file_in $file_out
    # elif [[ "$VAR" == "tas" && "$RUNMEAN_LEN" != "7" ]]; then
    #   echo "We should implement this first"; exit 1
      # file_in=$filepath_in/tas_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
      # cdo -setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "zg500" && "$RUNMEAN_LEN" == "7" ]]; then
      file_in=$filepath_in/zg500_day_CESM2-LE_${EXP}_r${ens_i}i1p1_native.RUNMEAN7.nc
      cdo -s setname,$VAR, -sellonlatbox,197,280,27,68, -remapcon2,$grid_cmip6ng, -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "zg500" && "$RUNMEAN_LEN" == "5" ]]; then
      file_in=$filepath_in/zg500_day_CESM2-LE_${EXP}_r${ens_i}i1p1_native.RUNMEAN5.nc
      cdo -s setname,$VAR, -sellonlatbox,197,280,27,68, -remapcon2,$grid_cmip6ng, -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "zg500" && "$RUNMEAN_LEN" == "1" ]]; then
      file_in=$filepath_in/zg500_day_CESM2-LE_${EXP}_r${ens_i}i1p1_native.nc
      cdo -s setname,$VAR, -sellonlatbox,197,280,27,68, -remapcon2,$grid_cmip6ng, -selvar,$CESM12_name $file_in $file_out
    # elif [[ "$VAR" == "zg500" && "$RUNMEAN_LEN" != "7" ]]; then
    #   echo "We should implement this first"; exit 1
      # file_in=$filepath_in/tas_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
      # cdo -setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    # elif [[ "$VAR" == "zg500" || "$VAR" == "psl" ]]; then
    #   if [[ "$ens_i" -ge "3" && "$ens_i" -le "8" ]]; then
    #     file_in=/net/xenon/climphys/jzeder/Projects/nonstationary-extremes/data/alternative_CESM12_input/z500_psl_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
    #   else 
    #     file_in=$filepath_in/z500_psl_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
    #   fi
    #   cdo -s setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    # elif [[ "$VAR" == "pr" && "$RUNMEAN_LEN" == "7" ]]; then
    #   file_in=$filepath_in/STATS/pr_mm_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.RUNMEAN7.nc
    #   cdo -s setname,$VAR, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    # elif [[ "$VAR" == "pr" && "$RUNMEAN_LEN" != "7" ]]; then
    #   file_in=$filepath_in/pr_mm_CESM12-LE_historical_r${ens_i}i1p1_${STARTYR}-2099.nc
    #   cdo -s setname,$VAR, -runmean,$rmlen, -remapcon2,$grid_cmip6ng -selvar,$CESM12_name $file_in $file_out
    elif [[ "$VAR" == "mrsol" ]]; then
      file_in=$filepath_in/mrsol_mon_CESM2-LE_${EXP}_r${ens_i}i1p1_native.nc
      cdo -s setname,$VAR, -sellonlatbox,197,280,27,68, -remapcon2,$grid_cmip6ng, -setrtomiss,-1000,0.00001, -vertsum, -sellevel,1/5, -selvar,$CESM12_name $file_in $file_out
    else
      echo "ERROR: A $VAR piControl file does not exist yet!"
      exit 1
    fi

  fi
}






## Loop over existing CMIP6ng datasets:
echo "      ----------- Retrieve CESM12 data -----------"
for MODEL in $MODELS
do
  if [ "$verbose" = "TRUE" ]; then echo "Working on model: $MODEL"; fi

  ## Get model-specific information:
  mod_lname=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f2 )
  mod_setting=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f3 )
  mod_exp_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f4 )
  mod_ensind_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f5 )
  mod_ensind_list=$( eval echo $mod_ensind_list )
  mod_soil_selection=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f6 )

  ## Get gridpoint size in 2.5° resolution:
  # get_fixed_var_CESM12
  ## WE GET THAT FROM THE CMIP6 DATABASE (copy-pasted from earlier project)
  
  ## Loop over variables:
  for VAR in $VARIABLES
  do
    if [ "$verbose" = "TRUE" ]; then echo "  - Working on variable: $VAR"; fi

    ## Get temporal/spatial resolution for different variables:
    tres_in=$( grep ^$VAR\| $model_info_file | cut -d"|" -f2 )
    CESM12_name=$( grep ^$VAR\| $model_info_file | cut -d"|" -f3 )
    tres_CMIP6_in=$( grep ^$VAR\| $model_info_file | cut -d"|" -f4 )

    ## Loop over experiments:
    for EXP in $mod_exp_list
    do
      if [ "$verbose" = "TRUE" ]; then echo "    - Working on experiment: $EXP"; fi
      if [ "$EXP" = "piControl" ]; then
        ens_i=1
        # get_runmean_CESM12_pictl
        ## WE GET THAT FROM THE CMIP6 DATABASE (copy-pasted from earlier project)

      else
        if [ "$verbose" = "TRUE" ]; then echo "      Loop over ensemble-members: $mod_ensind_list"; fi
        
        for ens_i in $mod_ensind_list
        do
          get_runmean_CESM2
        done

        ## Get ensemble means:
        # get_ensmean
        if [ "$VAR" != "tas" ]; then get_ensmean; fi
        ## We get tas ensemble means from monthly files
        ## For seasmeans of JJA/DFJ, switch off get_fldyearmean (line 593)
      fi
      
    done

  done

done

## Smooth piControl tas and detrend seasonal means of mrsol and zg500:
echo " ----------- Smooth pictl tas / Detrend mrsol/zg500 -----------"
for MODEL in $MODELS
do
  if [ "$verbose" = "TRUE" ]; then echo "Working on model: $MODEL"; fi

  ## Get model-specific information:
  mod_lname=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f2 )
  mod_setting=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f3 )
  mod_exp_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f4 )
  mod_ensind_list=$( grep ^$MODEL\| $model_info_file | cut -d"|" -f5 )

  ## Smooth tas of pictrl data:
  VAR=tas
  tres_in=day
  # get_smooth_pictl
  ## We did that manually

  ## Get pictrl seasonal base state of pictrl data:
  # get_seas_basestate_tas

  ## Loop over variables:
  for VAR in $VARIABLES
  do
    if [ "$verbose" = "TRUE" ]; then echo "  - Working on variable: $VAR"; fi

    ## Get temporal/spatial resolution for different variables:
    tres_in=$( grep ^$VAR\| $model_info_file | cut -d"|" -f2 )

    ## Get base state and linear trend with smoothed annual GMST
    get_seasonal_scaling_pattern
  done

done


## Send notification email:
duration=$SECONDS
mail -s "Retrieving 7-day running mean for CMIP6 models $MODELS and $VARIABLES ($(($duration / 60))min $(($duration % 60))s)" joel.zeder@env.ethz.ch < /dev/null
exit 0









