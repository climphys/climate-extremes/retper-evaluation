library(dplyr)
library(ncdf4)
library(ncdf4.helpers)
library(ggplot2)

yrmean_ls_CESM12 <- yrmean_ls_CESM2 <- yrmean_ls_ERA5 <- list()

## Get CESM12 data:
CESM12_input_path <- "/net/xenon/climphys/jzeder/Projects/gev-issues/data/CESM12/tas/day/g025/_FLDYRMEAN/_RUNMEAN4YR/"
for(i in 0:83) {
  filename=paste0("tas_day_CESM12-LE_historical_r",i,"i1p1_g025.RUNMEAN1.FLDYRMEAN.RUNMEAN4YR.nc")
  ncfile <- nc_open(file.path(CESM12_input_path,filename))
  tas  <- ncvar_get(ncfile, "tas")
  time <- as.integer(format(ncdf4.helpers::nc.get.time.series(ncfile),"%Y"))
  nc_close(ncfile)
  df <- data.frame(
    model="CESM12", exp="historical", ens=i,
    year=time,
    GMST_nonsmooth=tas)
  tasmean_1981_2010 <- df %>% filter(between(year,1981,2010)) %>%
    pull(GMST_nonsmooth) %>% mean()
  df <- df %>% mutate(GMST_nonsmooth_ANO=GMST_nonsmooth-tasmean_1981_2010)
  df$exp[df$year>=2015] <- "ssp585"
  yrmean_ls_CESM12[[paste0("CESM12_hist_",i)]] <- df
}

yrmean_df <- do.call(rbind,yrmean_ls_CESM12)
tasmean_1981_2010_all <- yrmean_df %>% filter(between(year,1981,2010)) %>%
  pull(GMST_nonsmooth) %>% mean()

filename=paste0("tas_day_CESM12-LE_piControl_r1i1p1_g025.RUNMEAN1.FLDYRMEAN.RUNMEAN4YR.nc")
ncfile <- nc_open(file.path(CESM12_input_path,filename))
tas  <- ncvar_get(ncfile, "tas")
time <- as.integer(format(ncdf4.helpers::nc.get.time.series(ncfile),"%Y"))
nc_close(ncfile)
df_pict <- data.frame(
  model="CESM12", exp="piControl", ens=1,
  year=time,
  GMST_nonsmooth=tas)
df_pict <- df_pict %>% mutate(GMST_nonsmooth_ANO=GMST_nonsmooth-tasmean_1981_2010_all)
yrmean_ls_CESM12[[paste0("CESM12_piControl")]] <- df_pict

## Get CESM2 data:
CESM2_input_path <- "/net/xenon/climphys/jzeder/Projects/gev-issues/data/CESM2/tas/day/g025/_FLDYRMEAN/_RUNMEAN4YR/"
for(i in 1:100) {
  filename=paste0("tas_day_CESM2-LE_histssp370_r",i,"i1p1_g025.RUNMEAN7.FLDYRMEAN.RUNMEAN4YR.nc")
  ncfile <- nc_open(file.path(CESM2_input_path,filename))
  tas  <- ncvar_get(ncfile, "tas")
  time <- as.integer(format(ncdf4.helpers::nc.get.time.series(ncfile),"%Y"))
  nc_close(ncfile)
  df <- data.frame(
    model="CESM2", exp="historical", ens=i,
    year=time,
    GMST_nonsmooth=tas)
  tasmean_1981_2010 <- df %>% filter(between(year,1981,2010)) %>%
    pull(GMST_nonsmooth) %>% mean()
  df <- df %>% mutate(GMST_nonsmooth_ANO=GMST_nonsmooth-tasmean_1981_2010)
  df$exp[df$year>2014] <- "ssp370"
  yrmean_ls_CESM2[[paste0("CESM12_hist_",i)]] <- df
}

yrmean_df <- do.call(rbind,yrmean_ls_CESM2)
tasmean_1981_2010_all <- yrmean_df %>% filter(between(year,1981,2010)) %>%
  pull(GMST_nonsmooth) %>% mean()

filename=paste0("tas_day_CESM2-LE_piControl_r1i1p1_g025.RUNMEAN7.FLDYRMEAN.RUNMEAN4YR.nc")
ncfile <- nc_open(file.path(CESM2_input_path,filename))
tas  <- ncvar_get(ncfile, "tas")
time <- as.integer(format(ncdf4.helpers::nc.get.time.series(ncfile),"%Y"))
nc_close(ncfile)
df_pict <- data.frame(
  model="CESM2", exp="piControl", ens=1,
  year=time,
  GMST_nonsmooth=tas)
df_pict <- df_pict %>% mutate(GMST_nonsmooth_ANO=GMST_nonsmooth-tasmean_1981_2010_all)
yrmean_ls_CESM2[[paste0("CESM2_piControl")]] <- df_pict


## Get ERA5 data:
ERA5_input_path <- "/net/xenon/climphys/jzeder/Projects/gev-issues/data/Reanalysis//tas/day/g025/_FLDYRMEAN/_RUNMEAN4YR/"
filename=paste0("tas_day_ERA5_reanalysis_r1i1p1_g025.RUNMEAN1.FLDYRMEAN.RUNMEAN4YR.nc")
ncfile <- nc_open(file.path(ERA5_input_path,filename))
tas  <- ncvar_get(ncfile, "tas")
time <- as.integer(format(ncdf4.helpers::nc.get.time.series(ncfile)-(60*60*6),"%Y"))
nc_close(ncfile)
df_ERA <- data.frame(
  model="ERA5", exp="reanalysis", ens=1,
  year=time,
  GMST_nonsmooth=tas)
tasmean_1981_2010_all <- df_ERA %>% filter(between(year,1981,2010)) %>%
  pull(GMST_nonsmooth) %>% mean()
df_ERA <- df_ERA %>% mutate(GMST_nonsmooth_ANO=GMST_nonsmooth-tasmean_1981_2010_all)
yrmean_ls_ERA5[[paste0("ERA5")]] <- df_ERA

## Get GISSTEMP data:
GISSTEMP_data <- read.csv("/net/xenon/climphys/jzeder/Projects/gev-issues/data/GLB.Ts_dSST.csv", header=T,skip=1,na.strings="***")
GISSTEMP_data_ann <- GISSTEMP_data
GISSTEMP_data_ann$Yearmean <- rowMeans(GISSTEMP_data_ann[,2:13])
# plot(Yearmean~Year,GISSTEMP_data_ann,t="l")

mean_1951_1980 <- GISSTEMP_data_ann %>%
  filter(between(Year,1951,1980)) %>%
  pull(Yearmean) %>%
  mean()

mean_1981_2010 <- GISSTEMP_data_ann %>%
  filter(between(Year,1981,2010)) %>%
  pull(Yearmean) %>%
  mean()


GISSTEMP_df <- GISSTEMP_data_ann
GISSTEMP_df <- GISSTEMP_df - (mean_1981_2010 - mean_1951_1980)
GISSTEMP_df$Year <- GISSTEMP_data_ann$Year

# ma <- function(x, n = 5){stats::filter(x, rep(1 / n, n), sides = 2)}
# ts_gisstemp <- ts(GISSTEMP_df$Yearmean, start = GISSTEMP_data_ann$Year[1])
ts_gisstemp_smooth <- stats::filter(GISSTEMP_df$Yearmean, c(0.5,1,1,1,0.5)/4, sides = 2)
GISSTEMP_df$Yearmean_smooth <- ts_gisstemp_smooth
plot(Yearmean~Year,GISSTEMP_df); lines(ts_gisstemp_smooth~Year,GISSTEMP_df)
plot(Yearmean+0.63 ~ Year,GISSTEMP_df); lines(ts_gisstemp_smooth+0.63 ~ Year,GISSTEMP_df)

## Put all of it together:
df_nonstmooth_GMST <- do.call(rbind, c(yrmean_ls_ERA5, yrmean_ls_CESM12,yrmean_ls_CESM2))
df_nonstmooth_GMST %>% filter(exp!="piControl", ens==1) %>% 
  ggplot(aes(x=year,y=GMST_nonsmooth_ANO,col=model)) + 
  geom_line() +
  geom_line(data=GISSTEMP_df, aes(x=Year,y=Yearmean_smooth),col="black")
dev.off()

save(df_nonstmooth_GMST, list=c("df_nonstmooth_GMST"),
     file="/net/xenon/climphys/jzeder/Projects/gev-issues/data/RData/Data_GMST_nonsmooth.RData")
save(GISSTEMP_df, list=c("GISSTEMP_df"),
     file="/net/xenon/climphys/jzeder/Projects/gev-issues/data/RData/Data_GISSTEMP_nonsmooth.RData")


