# README for retper-evaluation project

This is the (cleaned) git repository for the "The effect of a short observational record on the statistics of temperature extremes" paper by Zeder et al. (in review) published in Geophysical Research Letters. It is a close of the GitLab repository https://git.iac.ethz.ch/climphys/climate-extremes/retper-evaluation (publically accessible).

1. File structure:
- Settings (mostly plotting) in "settings.R" file
- GEV fitting and manipulation functions in "GEV_FUN.R"
- Data preparation in folder "1_data_prep" (Most things are hardcoded and can only be reproduced on IAC servers)
- GEV fitting procedures for large ensemble data in "2_GEVest_LE_fit"
- GEV evaluation procedures for large ensemble data in "3_GEVest_LE_eval"
- GEV fitting and evaluation procedures for synthetic data experiments in "4_GEVest_sim"
- Joint analysis (GEV parameter CI coverage) in "5_Joint_Analysis"

2. Data:
A set of pre-processed data is available on https://doi.org/10.3929/ethz-b-000619286. Aside from fevd objects (R lists), all the data is converted to .csv files for long-term readability (numerical values were rounded to four significant digits). The script "read_csv.R" can be used to read in the .csv file and assemble the data in an .RData file, which is later read by the individual scripts.

3. R Session:
The R code was produced on a Linux machine and in an R environment with the following specifications:
```
> sessionInfo()
R version 4.2.2 (2022-10-31)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: openSUSE Leap 15.4

Matrix products: default
BLAS/LAPACK: /usr/local/OpenBLAS-0.3.21/lib/libopenblas_haswellp-r0.3.21.so

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8     LC_MONETARY=en_US.UTF-8   
 [6] LC_MESSAGES=en_US.UTF-8    LC_PAPER=en_US.UTF-8       LC_NAME=C                  LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] grid      parallel  stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] mgcv_1.8-41        nlme_3.1-160       gtable_0.3.1       trend.acwd_2.4.0   scico_1.3.1        paletteer_1.5.0    ncdf4_1.19         abind_1.4-5       
 [9] evgam_1.0.0        ggpubr_0.5.0       pammtools_0.5.8    scales_1.2.1       RColorBrewer_1.1-3 ggplot2_3.4.0      readr_2.1.3        tidyr_1.2.1       
[17] dplyr_1.0.10       extRemes_2.1-3     distillery_1.2-1   Lmoments_1.3-1    

loaded via a namespace (and not attached):
 [1] Rcpp_1.0.9          mvtnorm_1.1-3       lattice_0.20-45     listenv_0.8.0       assertthat_0.2.1    digest_0.6.30       foreach_1.5.2      
 [8] utf8_1.2.2          parallelly_1.32.1   R6_2.5.1            backports_1.4.1     pillar_1.8.1        rlang_1.0.6         lazyeval_0.2.2     
[15] rstudioapi_0.14     car_3.1-1           Matrix_1.5-3        checkmate_2.1.0     labeling_0.4.2      splines_4.2.2       munsell_0.5.0      
[22] broom_1.0.1         compiler_4.2.2      numDeriv_2016.8-1.1 pkgconfig_2.0.3     globals_0.16.2      evd_2.3-6.1         tidyselect_1.2.0   
[29] tibble_3.1.8        prodlim_2019.11.13  codetools_0.2-18    fansi_1.0.3         future_1.29.0       crayon_1.5.2        tzdb_0.3.0         
[36] withr_2.5.0         timereg_2.0.4       lifecycle_1.0.3     DBI_1.1.3           magrittr_2.0.3      future.apply_1.10.0 cli_3.4.1          
[43] carData_3.0-5       farver_2.1.1        ggsignif_0.6.4      ellipsis_0.3.2      generics_0.1.3      vctrs_0.5.1         cowplot_1.1.1      
[50] boot_1.3-28.1       Formula_1.2-4       lava_1.7.0          rematch2_2.1.2      iterators_1.0.14    tools_4.2.2         glue_1.6.2         
[57] purrr_0.3.5         hms_1.1.2           pec_2022.05.04      survival_3.4-0      colorspace_2.0-3    rstatix_0.7.1 
```
