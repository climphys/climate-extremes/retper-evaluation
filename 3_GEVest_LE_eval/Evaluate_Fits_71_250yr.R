#!/bin/Rscript

###### HEADER - Evaluate_Fits_71_251.R #######################################
# Author:  Joel Zeder (joel.zeder@env.ethz.ch)
# Date:    09.06.2022
# Purpose: Evaluate GEV fits on time series subsets (1850-2100 and 1950-2020).
##############################################################################

rm(list=ls())
script_path  <- "<entre your script path here>"
setwd(script_path)

project_path <- "<entre your output path here>"
plot_path     <- paste0(project_path,"plots/Evaluation_retper/")
data_path     <- paste0(project_path,"data/")
dir.create(data_path, showWarnings = F, recursive = T)
dir.create(plot_path, showWarnings = F, recursive = T)

#### LOAD LIBRARIES & SOURCE SCRIPTS #########################################
{
  suppressWarnings(library(pammtools))
  suppressWarnings(library(ncdf4))
  suppressWarnings(library(dplyr))
  suppressWarnings(library(extRemes))
  suppressWarnings(library(ggplot2))
  suppressWarnings(library(parallel))
  suppressWarnings(library(ggpubr))
  suppressWarnings(library(paletteer))
  suppressWarnings(library(scico))
  suppressWarnings(library(RColorBrewer))
  suppressWarnings(library(tidyr))
  suppressWarnings(library(trend.acwd))
  suppressWarnings(library(gtable))
  suppressWarnings(library(grid))
  suppressWarnings(library(mgcv))
}
filter   <- dplyr::filter
select   <- dplyr::select
collapse <- dplyr::collapse

source("settings.R")
source("GEV_FUN.R")
source("4_GEVest_sim/Sim_Static/Simulation_GEV_Evaluations_FUN.R")
source("3_GEVest_LE_eval/Evaluate_Fits_71yrSubset_FUN.R")
source("3_GEVest_LE_eval/Evaluate_Fits_71_250yr_FUN.R")

#### SETTINGS ###############################################################
options(dplyr.summarise.inform = FALSE)

#### LOAD DATA ##############################################################

## Load input data (raw PNW extremes data / GEV fits on full datasets with
## model objects):
load(file.path(data_path,"Input_data_GEV_est.RData"))

#### MAIN BODY ##############################################################
## Define reference year and GMST covariate:
ref_year <- 2021
GMST_covar <- "GMST_smooth_ANO"

## Get the reference XXX-year return level for 2021:
AEP_ref <- 1/100

## Get GMST covariate
gmst_values_ref <- VAR_df_1d %>% 
  filter(model!="ERA5",#model==model_i, 
         exp!="piControl", max_index=="Tx1d", year==ref_year) %>% 
  select(ens, model, GMST_nonsmooth_ANO, GMST_smooth_ANO) 
gmst_values_ref$GMST_2021 <- gmst_values_ref[,GMST_covar]


##---------------------------------------------------------------------------
##  1) Get return level from non-stationary GEV:
gmst_rlvl_values_ref <- gev_ci_all_df %>% 
  filter(model!="ERA5", max_index=="Tx1d", BM==1) %>% 
  full_join(gmst_values_ref,by="model") %>% #character()) %>% 
  mutate(rlvl_100_ref=qevd_vec(1-AEP_ref,loc=mu0+mu1*GMST_smooth_ANO,
                               scale=scale,shape=shape,type="GEV")) %>% 
  select(model, ens, method, rlvl_100_ref, GMST_2021) %>% 
  mutate(rlvl_method="nonstationary")

##---------------------------------------------------------------------------
## Collect estimates across various estimation settings (short / long est.
## period, using the smooth and non-smooth GMST covariate, etc.):

rper_all_2100 <- gev_ci_all_df  %>%
  filter(#model==model_i, 
         max_index=="Tx1d", BM==1, method!="GMLE") %>% 
  full_join(gmst_rlvl_values_ref,by=c("model","method")) %>% #character()) %>% 
  mutate(rper_2021_ref=1/(1-pevd_vec(rlvl_100_ref,mu0+mu1*GMST_2021,scale,shape,type="GEV")),
         type="smooth_noprior_all_1850_2099")

rper_all_2020 <- gev_ci_2020_df  %>%
  filter(#model==model_i, 
         max_index=="Tx1d", BM==1, method!="GMLE") %>% 
  full_join(gmst_rlvl_values_ref,by=c("model","method")) %>% #character()) %>% 
  mutate(rper_2021_ref=1/(1-pevd_vec(rlvl_100_ref,mu0+mu1*GMST_2021,scale,shape,type="GEV")),
         type="smooth_noprior_all")


## Add non-smooth estimates:
rper_indi_nonsmooth_2020 <- GEV_res_nonsmooth_df %>%
  filter(#model==model_i,
         max_index=="Tx1d", BM==1,
         year_end==2020) %>%
  inner_join(gmst_rlvl_values_ref,by=c("ens","model","method")) %>% #) %>%
  mutate(rper_2021_ref=1/(1-pevd_vec(rlvl_100_ref,mu0+mu1*GMST_2021,scale,shape,type="GEV")))

rper_indi_nonsmooth_2100 <- GEV_res_nonsmooth_1850_2099_df %>%
  filter(#model==model_i,
         max_index=="Tx1d", BM==1,
         year_start==1850) %>%
  inner_join(gmst_rlvl_values_ref,by=c("ens","model","method")) %>% #) %>%
  mutate(rper_2021_ref=1/(1-pevd_vec(rlvl_100_ref,mu0+mu1*GMST_2021,scale,shape,type="GEV")))


rper_values_nonsmooth <- bind_rows(rper_all_2100, rper_all_2020,
                                   rper_indi_nonsmooth_2020,
                                   rper_indi_nonsmooth_2100) %>% 
  mutate(type=factor(type),
         method=factor(method, levels=c("MLE","Bayesian"), ordered=T)) %>% 
  filter(method!="GMLE", model!="ERA5")

##----------------------------------------------------------------------------
## Make boxplot of return periods and scatter plots of parameter differences:
## Get quantiles of differences:
model_sel <- "CESM12"
par_ref <- rper_values_smooth %>%
  filter(type=="smooth_noprior_all_1850_2099") %>% 
  group_by(model, method) %>% 
  summarise(mu0_ref  =unique(mu0),
            mu1_ref  =unique(mu1),
            scale_ref=unique(scale),
            shape_ref=unique(shape))

box_scatter_df <- rper_values_nonsmooth %>% 
  filter(type %in% c("nonsmooth_noprior", "nonsmooth_noprior_1850_2099")) %>% 
  left_join(par_ref, by=c("model","method")) %>% 
  mutate(mu0_diff   = mu0 - mu0_ref,
         mu1_diff   = mu1 - mu1_ref,
         scale_diff = scale - scale_ref,
         shape_diff = shape - shape_ref,
         method     = factor(method, levels=c("MLE","Bayesian"), ordered=T)) %>% 
  mutate(rlvl_100_2021      = qevd_vec(if_else(BM==1,0.99, if_else(BM==5,(1-0.01)^5,(1-0.01)^10)),
                                      mu0+mu1*rlvl_covar,scale,shape,type="GEV"),
         rlvl_Inf_2021      = mu0+mu1*rlvl_covar-scale/shape,
         # rper_rlvl_100_ref_2021 = 1/(1-pevd_vec(rlvl_100_ref, mu0+mu1*GMST_2021,scale,shape,type="GEV"))
         ) %>%
  mutate(rper_rlvl_100_ref=rper_2021_ref)

## Evaluate return periods and plot the associated parameter estimates:
type_levels <- c("nonsmooth_noprior","nonsmooth_noprior_1850_2099")
type_labels <- c(paste("183 indiv. GEV fits (1950-2020)"),
                 paste("183 indiv. GEV fits (1850-2100)"))
names(type_labels) <- type_levels
return_typelabelnames <- function(x) return(type_labels[[x]])

AEP_ref     <- 1/100
ulim_larger <- 1000
mu1_xaxis_label <- bquote(""~widehat(mu)["GMST"]-mu["GMST,ref"]~"[°C °"*C^-1*"]")
shp_yaxis_label <- bquote(""~widehat(xi)-xi[ref]~"[ ]")
shp_mu1_plot    <- box_scatter_plot(box_scatter_df,"mu1_diff","shape_diff",
                                    mu1_xaxis_label, shp_yaxis_label,
                                    strokecol_points="grey40")

sigma_xaxis_label <- bquote(""~widehat(sigma)-sigma["ref"]~"[°C]")
shp_sigma_plot    <- box_scatter_plot(box_scatter_df,"scale_diff","shape_diff",
                                      sigma_xaxis_label, shp_yaxis_label,
                                      strokecol_points="grey40")

## Plot parameter estimates scatter plots (off-diagonal ML/Bayesian only,
## on the diagonal compare MLE and Bayesian)
allparam_scatter <- box_scatter_df %>%
  mutate(ens_model=paste0(ens,"_",model)) %>% 
  allparam_scatter_plot(obs_factor = "ens_model")

## Evaluate QQ-plot of the GEV fits in each ensemble member:
varlist   <- c("VAR_df_1d","plot_path")
cl        <- makeCluster(30)
clusterEvalQ(cl,library(dplyr))
clusterEvalQ(cl,library(extRemes))
clusterEvalQ(cl,tools::psnice(value = 15))
clusterExport(cl,varlist)
clusterSetRNGStream(cl)

ens_CESM12 <- VAR_df_1d %>% filter(model=="CESM12") %>% pull(ens) %>% unique()
ens_CESM2  <- VAR_df_1d %>% filter(model== "CESM2") %>% pull(ens) %>% unique()
parLapply(cl, ens_CESM12, qq_plot_SMILE, model_i="CESM12")
parLapply(cl,  ens_CESM2, qq_plot_SMILE, model_i="CESM2")

## =============================================================================
## Add bootstrap CI to box_scatter_df
box_scatter_boot_df <- box_scatter_df %>% add_MLE_boot_CI(VAR_df_1d)

## =============================================================================
## Determine and plot non-coverage of CI fraction:
rlvl_rper_scatter_plot(box_scatter_boot_df, method_sel="MLE", model_sel="CESM12")
rlvl_rper_scatter_plot(box_scatter_boot_df, method_sel="Bayesian", model_sel="CESM12")
rlvl_rper_scatter_plot(box_scatter_boot_df, method_sel="MLE", model_sel="CESM2")
rlvl_rper_scatter_plot(box_scatter_boot_df, method_sel="Bayesian", model_sel="CESM2")

## =============================================================================
## Make methods plot with LE data:
ens_sel    <- 5
method_sel <- "MLE"
model_sel  <- "CESM12"

GEV_est_df <- box_scatter_df %>% 
  filter(method==method_sel, model==model_sel, ens==ens_sel) %>% 
  mutate(param_i=ens, sample_i=ens)
sample_all_df <- VAR_df_1d %>% 
  filter(model==model_sel, ens==ens_sel, max_index=="Tx1d") %>% 
  mutate(param_i=ens, sample_i=ens,
         mu0  =unique(GEV_est_df$mu0_ref),
         mu1  =unique(GEV_est_df$mu1_ref),
         scale=unique(GEV_est_df$scale_ref),
         shape=unique(GEV_est_df$shape_ref),
         location=mu0+mu1*GMST_nonsmooth_ANO,
         type =if_else(year>=1950|year<=2020,"nonsmooth_noprior",
                       "nonsmooth_noprior_1850_2099"),
         Data_short=FALSE, loglik=NA)
methods_plot(data.frame(param_i=ens_sel,sample_i=ens_sel))

## Plot case similar to PNW 2021 event =========================================
box_scatter_df %>%
  filter(model=="CESM12", method=="MLE",
         type=="nonsmooth_noprior",
         rper_val1>10e3) %>%
  pull(ens)
  ## ==> From here, continue in 1_Get_Ulim_estimate.R script
  
## Fit Bayesian GEV fits and see how much a difference it makes taking
## the Posterior mean/median/mode 100-year return level:
data_df <- gev_ls$CESM12$Tx1d$BM1$All$BAYES$cov.data
get_Bayes_fit <- function(ens_i) {
  data_ens_df <- data_df %>% filter(ens==ens_i)
  GMST_2021   <- data_ens_df %>% filter(year==2021) %>% pull(GMST_smooth_ANO)
  data_ens_df <- data_ens_df %>% filter(between(year,1950,2020))
  GEV_bayes   <- fevd(data_ens_df$Tx, data=data_ens_df,
                      location.fun=~GMST_smooth_ANO,
                      type="GEV", method="Bayes")
  qcov_2021  <- make.qcov(GEV_bayes, vals=list(mu1=GMST_2021))
  median2021 <- findpars.fevd.bayesian(GEV_bayes, FUN="median",qcov=qcov_2021)
  mean2021   <- findpars.fevd.bayesian(GEV_bayes, FUN="mean",  qcov=qcov_2021)
  rlvl100_2021_mean   <- qevd(0.99, sum(mean2021$location),
                              mean2021$scale, mean2021$shape)
  rlvl100_2021_median <- qevd(0.99, sum(median2021$location),
                              median2021$scale, median2021$shape)
  return(c(mean=rlvl100_2021_mean, median=rlvl100_2021_median))
}
rlvl_100_Bayes <- sapply(unique(data_df$ens), get_Bayes_fit)

rlvl_bayes_comp_plot <- data.frame(val=c(rlvl_100_Bayes),
                                   type=c("mean","median")) %>% 
  ggplot() +
  geom_violin(aes(x=type, y=val, col=type, fill=type), alpha=0.2,
              draw_quantiles = c(.05,.25,.5,.75,.95)) +
  geom_point(aes(x=type, y=val, col=type), alpha=0.7, #size=0.6,
             position = position_jitter(seed = 1, width = 0.2)) +
  scale_y_continuous(name=bquote("100-year return level "*widehat(z)["p=0.01"]),
                     labels = function(x) paste0(x,"°C")) +
  scale_color_manual(values=c(mean="chocolate4",median="cyan4"), name="Posterior") +
  scale_fill_manual(values=c(mean="chocolate4",median="cyan4"), name="Posterior") +
  theme(axis.title.x = element_blank())
rlvl_bayes_comp_plot

rlvl_bayes_comp_plot2 <- data.frame(t(rlvl_100_Bayes)-273.15) %>% 
  ggplot() +
  geom_point(aes(x=median, y=mean), alpha=0.7) +
  geom_abline(intercept=0,slope=1,linetype="longdash") +
  scale_y_continuous(name=bquote("Posterior Mean "*widehat(z)["p=0.01"]),
                     labels = function(x) paste0(x,"°C")) +
  scale_x_continuous(name=bquote("Posterior Median "*widehat(z)["p=0.01"]),
                     labels = function(x) paste0(x,"°C"))
rlvl_bayes_comp_plot2

save_plot(rlvl_bayes_comp_plot2, file.path(plot_path, "Bayes_rlvl100_comparison"),
          width = 4.5, height = 4.5)







